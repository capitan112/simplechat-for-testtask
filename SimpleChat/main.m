//  main.m
//  SimpleChat
//
//  Created by Капитан on 15.11.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
