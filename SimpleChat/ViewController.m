//
//  ViewController.m
//  SimpleChat
//
//  Created by Капитан on 15.11.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "PoetryGenerator.h"
#import "Messages.h"


static NSString *const hashTag = @"#awesome_app";
static const float serverTimeInterval = 20;
static const float keyboardHeight = 44;
static const float keyboardY = 260;
static const float contentHeight = 32;
static const float contentWidth = 245;

@interface ViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UITextField *inputTextField;
@property (nonatomic, strong) AppDelegate *application;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchRequest *request;
@property (nonatomic,  strong) NSMutableArray *chatMessageArray;
@property (nonatomic) BOOL isScrolling;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadManagedObjectContext];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self messagesFromDatabase];
    [NSTimer scheduledTimerWithTimeInterval:serverTimeInterval
                                     target:self
                                   selector:@selector(serverEmulation)
                                   userInfo:nil
                                    repeats:YES];
    self.isScrolling = YES;
    [self createToolBar];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showKeyboard:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideKeyboard:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView reloadData];
    [self.tableView reloadData];
    [self autoScrollTableView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)autoScrollTableView {
    int lastRowNumber = (int)[self.tableView numberOfRowsInSection:0] - 1;
    if (lastRowNumber > 0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:lastRowNumber inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}


#pragma mark - init data

- (void)createToolBar {
    [self.navigationController setToolbarHidden:NO];
    self.inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, contentWidth, contentHeight)];
    self.inputTextField.delegate = self;
    self.inputTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.inputTextField.borderStyle = UITextBorderStyleRoundedRect;
    self.inputTextField.placeholder = @"Message";
    UIBarButtonItem *buttonSend = [[UIBarButtonItem alloc]initWithTitle:@"Send"
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(sendMessagePressed)];
    UIBarButtonItem *textFieldItem = [[UIBarButtonItem alloc] initWithCustomView:self.inputTextField];
    NSArray *toolbarItems = [[NSArray alloc] initWithObjects: textFieldItem, buttonSend, nil];
    [self setToolbarItems:toolbarItems animated:NO];
}

- (void)reloadTableView {
    if (self.isScrolling) {
        [self.tableView setContentOffset:CGPointMake(0, CGFLOAT_MAX)];
    }
    [self.tableView reloadData];
    [self saveMassageToDatabase];
}

- (void)loadManagedObjectContext {
    self.application = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    self.managedObjectContext = self.application.managedObjectContext;
}

#pragma mark - Core data init

- (void)messagesFromDatabase {
    if (!self.request) {
       [self fetchRequestGenerator];
    }
    NSError *fetchError;
    NSUInteger count = [self.managedObjectContext countForFetchRequest:self.request error:&fetchError];
    if (count) {
        Messages *messages = [[self.managedObjectContext executeFetchRequest:self.request
                                                                       error:&fetchError]
                                                               objectAtIndex:0];
        if (messages.info) {
            self.chatMessageArray = [NSKeyedUnarchiver unarchiveObjectWithData:messages.info];
        }
    } else {
        self.chatMessageArray = [[NSMutableArray alloc] init];
    }
}

- (void)fetchRequestGenerator {
    self.request = [NSFetchRequest fetchRequestWithEntityName:@"Messages"];
    [self.request setPredicate:[NSPredicate predicateWithFormat:@"key = %@", @"array"]];
    [self.request setFetchLimit:1];
}

- (void)saveMassageToDatabase {
    if (!self.request) {
        [self fetchRequestGenerator];
    }
    NSError *fetchError;
    NSUInteger count = [self.managedObjectContext countForFetchRequest:self.request error:&fetchError];
    Messages *messages;
    if (count) {
        messages = [[self.managedObjectContext executeFetchRequest:self.request
                                                             error:&fetchError]
                                                     objectAtIndex:0];

    } else {
        messages = (Messages *)[NSEntityDescription insertNewObjectForEntityForName:@"Messages"
                                                             inManagedObjectContext:self.managedObjectContext];
    }
    NSData *arrayData = [NSKeyedArchiver archivedDataWithRootObject:self.chatMessageArray];
    messages.key = @"array";
    messages.info = arrayData;
    [self.application saveContext];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.chatMessageArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSString *message = [self assebleMessageByPathIndex:indexPath.row];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc]initWithString:message];
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceCharacterSet];
    NSArray *words = [message componentsSeparatedByCharactersInSet:whiteSpace];
    
    for (NSString *word in words) {
        if ([word isEqualToString:hashTag]) {
            NSRange range = [message rangeOfString:word];
            [attributedText addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        }
    }
    NSRange boldedTextLocationFrom = [message rangeOfString:@"]"];
    NSRange boldedTextLocationTo = [message rangeOfString:@": "];
    NSRange boldedTextRange = NSMakeRange(boldedTextLocationFrom.location + 1,
                                          boldedTextLocationTo.location - boldedTextLocationFrom.location);
    UIFont *boldFont = [UIFont fontWithName:@"Helvetica-Bold" size:14.0f];
    [attributedText addAttribute:NSFontAttributeName value:boldFont range:boldedTextRange];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = message;
    cell.textLabel.attributedText = attributedText;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *message = [self assebleMessageByPathIndex:indexPath.row];
    CGRect textSize = [self MessageSize:message];
    
    return textSize.size.height;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (endScrolling >= scrollView.contentSize.height) {
        self.isScrolling = YES;
    } else {
        self.isScrolling = NO;
    }
}

#pragma mark Assembling Message

- (NSString *)assebleMessageByPathIndex:(NSInteger)pathIndex {
    NSString *date = [[self.chatMessageArray objectAtIndex:pathIndex] objectForKey:@"time"];
    NSString *nickName = [[self.chatMessageArray objectAtIndex:pathIndex] objectForKey:@"nickname"];
    NSString *message = [[self.chatMessageArray objectAtIndex:pathIndex] objectForKey:@"message"];
    NSMutableString *assebleMessage = [NSMutableString stringWithString:date];
    [assebleMessage appendString:@" "];
    [assebleMessage appendString:nickName];
    [assebleMessage appendString:@": "];
    [assebleMessage appendString:message];
    
    return assebleMessage;
}

- (CGRect)MessageSize:message {
    CGSize maximumLabelSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, CGFLOAT_MAX);
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:16.0f];
    NSDictionary *options = @{ NSFontAttributeName: font};
    CGRect textSize = [message boundingRectWithSize:maximumLabelSize
                                            options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                         attributes:options
                                            context:nil];
    return textSize;
}

#pragma mark TextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self.inputTextField.text length] != 0) {
        [self addMessage:self.inputTextField.text user:@"TurboUser"];
        self.inputTextField.text = @"";
    }

    [self.inputTextField resignFirstResponder];
    return YES;
}

- (void)addMessage:(NSString *)message user:(NSString *)user {
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setTimeStyle:NSDateFormatterMediumStyle];
    timeFormatter.dateFormat = @"[HH:mm]";
    NSString *time = [timeFormatter stringFromDate:currentTime];
    NSDictionary *messageDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:time, @"time", user, @"nickname", message, @"message", nil];
    [self.chatMessageArray addObject: messageDictionary];
    [self reloadTableView];
}

- (void)sendMessagePressed {
    if ([self.inputTextField.text length] != 0) {
        [self addMessage:self.inputTextField.text user:@"TurboUser"];
        self.inputTextField.text = @"";
    }
    [self.inputTextField resignFirstResponder];
}

- (void)serverEmulation {
    PoetryGenerator *randomPoetry;
    if (!randomPoetry) {
        randomPoetry = [[PoetryGenerator alloc] init];
    }
    
    [self addMessage:[randomPoetry randomPoetryCreation] user:@"Рифмоплет"];
}

#pragma mark Keyboard Notification

- (void)showKeyboard:(NSNotification*)notification {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.3];
    [self.navigationController.toolbar setFrame:CGRectMake(0,
                                       [UIScreen mainScreen].bounds.size.height - keyboardY,
                                       [UIScreen mainScreen].bounds.size.width,
                                       keyboardHeight)];
    [UIView commitAnimations];
}

- (void)hideKeyboard:(NSNotification*)notification {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration: 0.3];
    [self.navigationController.toolbar setFrame:CGRectMake(0,
                                       [UIScreen mainScreen].bounds.size.height - keyboardHeight,
                                       [UIScreen mainScreen].bounds.size.width,
                                       keyboardHeight)];
    [UIView commitAnimations];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
