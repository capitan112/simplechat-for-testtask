//
//  PoetryGenerator.h
//  SimpleChat
//
//  Created by Капитан on 15.11.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PoetryGenerator : NSObject

- (NSString *)randomPoetryCreation;

@end
