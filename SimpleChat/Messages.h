//
//  Messages.h
//  SimpleChat
//
//  Created by Капитан on 17.11.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Messages : NSManagedObject

@property (nonatomic, retain) NSData * info;
@property (nonatomic, retain) NSString * key;

@end
