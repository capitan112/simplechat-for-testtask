//
//  ViewController.h
//  SimpleChat
//
//  Created by Капитан on 15.11.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end

