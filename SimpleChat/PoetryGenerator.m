//
//  PoetryGenerator.m
//  SimpleChat
//
//  Created by Капитан on 15.11.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "PoetryGenerator.h"

@interface PoetryGenerator ()

@property (nonatomic,  strong) NSArray *firstLineColumn1;
@property (nonatomic,  strong) NSArray *firstLineColumn2;
@property (nonatomic,  strong) NSArray *firstLineColumn3;
@property (nonatomic,  strong) NSArray *firstLineColumn4;
@property (nonatomic,  strong) NSArray *firstLineColumn5;
@property (nonatomic,  strong) NSArray *secondLineColumn1;
@property (nonatomic,  strong) NSArray *secondLineColumn2;
@property (nonatomic,  strong) NSArray *secondLineColumn3;
@property (nonatomic,  strong) NSArray *secondLineColumn4;
@property (nonatomic,  strong) NSArray *secondLineColumn5;

@end

@implementation PoetryGenerator

- (void)dataBaseLoading {
    self.firstLineColumn1 = @[@"я помню", @"не помню", @"забыть бы", @"купите", @"проспите", @"какое", @"угробил", @"хреново", @"открою", @"ты чуешь"];
    self.firstLineColumn2 = @[@"чудное", @"странное", @"некое", @"вкусное", @"пьяное", @"свинское", @"четкое", @"банное", @"нужное", @"конское"];
    self.firstLineColumn3 = @[@"мгновение", @"затмение", @"хотенье", @"варенье", @"творенье", @"везенье", @"рожденье", @"смещенье", @"печенье", @"ученье", @"мученье"];
    self.firstLineColumn4 = @[@"передомной", @"под косячком", @"на кладбище", @"в моих мечтах", @"под скальпилем", @"в моих руках", @"из-за угла", @"в моих ушах", @"в ночном горшке", @"из головы"];
    self.firstLineColumn5 = @[@"явилась ты", @"добилась ты", @"торчат кресты", @"стихов листы", @"забилась ты", @"ворчат деды", @"поют дрозды", @"из темноты", @"пробилась ты", @"поют глисты"];
    self.secondLineColumn1 = @[@"мимолетное", @"детородное", @"психотропное", @"кайфоломное", @"очевидное", @"у воробушков", @"эдакое вот", @"нам не чуждое", @"благородное", @"сиропное"];
    self.secondLineColumn2 = @[@"виденье", @"сиденье", @"паренье", @"сужденье", @"вращение", @"ношение", @"смятенье", @"теченье", @"паденье", @"сплетенье"];
    self.secondLineColumn3 = @[@"гений", @"сторож", @"символ", @"спарта", @"правда", @"ангел", @"водка", @"пиво", @"ахтуг"];
    self.secondLineColumn4 = @[@"чистой", @"вечной", @"тухлой", @"просит", @"грязной", @"липкой", @"мутной", @"в пене", @"женской", @"жаждет"];
    self.secondLineColumn5 = @[@"красоты", @"мерзлоты", @"суеты", @"наркоты", @"срамоты", @"боброты", @"типа ты", @"простоты", @"наготы"];
}

- (int)random:(NSArray *)array {
    int randomNumber;
    
    randomNumber = arc4random_uniform((int)[array count]);
    return randomNumber;
}

- (NSString *)randomPoetryCreation {
    [self dataBaseLoading];
    NSString *firstWord = [NSString stringWithFormat:@"%@", self.firstLineColumn1[[self random:self.firstLineColumn1]]];
    NSString *secondWord = [NSString stringWithFormat:@"%@", self.firstLineColumn2[[self random:self.firstLineColumn2]]];
    NSString *thirdWord = [NSString stringWithFormat:@"%@", self.firstLineColumn3[[self random:self.firstLineColumn3]]];
    NSString *fourthWord = [NSString stringWithFormat:@"%@", self.firstLineColumn4[[self random:self.firstLineColumn4]]];
    NSString *fivethWord = [NSString stringWithFormat:@"%@", self.firstLineColumn5[[self random:self.firstLineColumn5]]];
    NSString *firstWordLine2 = [NSString stringWithFormat:@"%@", self.secondLineColumn1[[self random:self.secondLineColumn1]]];
    NSString *secondWordLine2 = [NSString stringWithFormat:@"%@", self.secondLineColumn2[[self random:self.secondLineColumn2]]];
    NSString *thirdWordLine2 = [NSString stringWithFormat:@"%@", self.secondLineColumn3[[self random:self.secondLineColumn3]]];
    NSString *fourtWordLine2 = [NSString stringWithFormat:@"%@", self.secondLineColumn4[[self random:self.secondLineColumn4]]];
    NSString *fivethWordLine2 = [NSString stringWithFormat:@"%@", self.secondLineColumn5[[self random:self.secondLineColumn5]]];
    NSString *resultPoetry = [NSString stringWithFormat:@"%@ %@ %@, %@ %@, как %@ %@, как %@ %@ %@.", firstWord, secondWord, thirdWord, fourthWord, fivethWord, firstWordLine2, secondWordLine2, thirdWordLine2, fourtWordLine2, fivethWordLine2];
    
   return resultPoetry;
}


@end
